#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smc_library/smc_orfanidis.h"
#include "smc_library/smc.h"

#define output_file_y1 "data/wave1_F1.txt" 
#define output_file_y2 "data/wave1_F2.txt" 
#define output_file_y3 "data/wave1_F3.txt" 
int main()
{
int D, N;
double *w, m, A, f1, f2, f3, q1, q2, q3;
D = 1000; N = D*2;
double y1[N],y2[N],y3[N];
w = (double*)calloc(D,sizeof(double));

q1 = q2 = q3 = 0;
A = 1;
f1 = 1.0/D;
f2 = 5.0/D;

int i;
for (i = 0; i < D; i++)
{
	w[(int)q1]	= sine(D,i);
	gdelay2(D-1, 1.0, &q1);
}

m=D/4;
gdelay2(D-1, -m*D*f2, &q3); //Delay sine3 with pi/4. (q3 will be shifted) 

/* Create Wave Signals */
int n;
for (n=0; n < N; n++)
{
	y1[n] = wavgeni(D, w, A, f1, &q1);
	y2[n] = wavgeni(D, w, A, f2, &q2);
	y3[n] = wavgeni(D, w, A, f2, &q3);
}
	

/* FILE OUTPUTS */
int len;
len = sizeof(y1)/sizeof(y1[0]); smc_write_file_float(y1,output_file_y1, len);
len = sizeof(y2)/sizeof(y2[0]); smc_write_file_float(y2,output_file_y2, len);
len = sizeof(y3)/sizeof(y3[0]); smc_write_file_float(y3,output_file_y3, len);

return 0;
}



