/* conv.c - convolution of x[n] with h[n], resulting in y[n] */
#ifndef max
#define max(a,b)  (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b)  (((a) < (b)) ? (a) : (b))
#endif

#include <math.h>
/* Convolution function*/
void conv(int M, double *h, int L, double *x, double *y)
{
    int n, m;
        for (n = 0; n < L+M; n++)
            for (y[n] = 0, m = max(0, n-L+1); m <= min(n, M); m++)
                y[n] += h[m] * x[n-m];
}

/* -------------------------------------------------------- */
/*						WAVETABLES							*/
/* -------------------------------------------------------- */
/* gdelay2.c - generalized circular delay with real-valued shift */

void gdelay2(int D, double c, double *q)
{                          					/* \(c\)=shift, \(q\)=offset index */
       *q -= c;                           	/* decrement by \(c\) */
       if (*q < 0)  
              *q += D+1;
       if (*q > D)
              *q -= D+1;
}

/* trapez.c - trapezoidal wavetable: D1 rising, D2 steady */

double trapez(int D, int D1, int D2, int i)
{
       if (i < D1)
              return i/(double) D1;
       else
            if (i < D1+D2)
                 return 1;
            else
                 return (D - i)/(double) (D - D1 - D2);
}

/* square.c - square wavetable of length D, with D1 ones */
double square(int D1, int i)
{
       if (i < D1)
              return 1;
       else
              return 0;
}

/* sine.c - sine wavetable of length D 	*/
			#include <math.h> 			
double sine(int D, int i)
{
       double pi = 4 * atan(1.0);

       return sin(2 * pi * i / D);
}

/* wavgeni.c - wavetable generator (interpolation method) */
/*       	void gdelay2(); 			*/
/* wavgen.c - wavetable generator (truncation method) */
/*void gdelay2();*/
double wavgen(int D, double *w, double A, double F, double *q)      /* usage: y = wavgen(D, w, A, F, &q); */
{                              										/* \(D\) = wavetable length */                													
																	/* \(A\) = amplitude, \(F\) = frequency, \(q\) = offset index */
       double y;
       int i;

       i = (int) (*q);                             /* truncate down */

       y = A * w[i];

       gdelay2(D-1, D*F, q);                       /* shift  \(c = DF\) */

       return y;
}
/* wavgenr.c - wavetable generator (rounding method) */

/* void gdelay2(); */

double wavgenr(int D, double *w, double A, double F, double *q)     /* usage: y = wavgenr(D, w, A, F, &q); */
{					                								/* \(D\) = wavetable length */
					                								/* \(A\) = amplitude, \(F\) = frequency, \(q\) = offset index */
       double y;
       int k;

       k = (int) (*q + 0.5);                     /* round */

       y = A * w[k];

       gdelay2(D-1, D*F, q);                     /* shift  \(c = DF\) */

       return y;
}

double wavgeni(int D, double *w, double A, double F, double *q)     /* usage: y = wavgeni(D, w, A, F, &q); */                             /* \(D\) = wavetable length */
{               														/* \(A\) = amplitude, \(F\) = frequency, \(q\) = offset index */
       double y;
       int i, j;

       i = (int) *q;                        /* interpolate between \(w[i], w[j]\) */
       j = (i + 1) % D;                     

       y = A * (w[i] + (*q - i) * (w[j] - w[i]));

       gdelay2(D-1, D*F, q);                     /* shift  \(c = DF\) */

       return y;
}


/* -------------------------------------------------------- */
/*				  END	 WAVETABLES							*/
/* -------------------------------------------------------- */

/* -------------------------------------------------------- */
/*				           REVERB							*/
/* -------------------------------------------------------- */

/* tap.c - i-th tap of circular delay-line buffer */

double tap(int D, double *w, double *p, int i)                    /* usage: si = tap(D, w, p, i); */
{
    return w[(p - w + i) % (D + 1)];
}

/* tap2.c - i-th tap of circular delay-line buffer */

double tap2(int D, double *w, int q, int i)                   /* usage: si = tap2(D, w, q, i); */
{
    return w[(q + i) % (D + 1)];
}

/* tapi2.c - interpolated tap output of a delay line */

double tapi2(int D, double *w, int q, double d)                  /* usage: sd = tapi2(D, w, q, d); */
{
    int i, j;
    double si, sj;

    i = (int) d;                       /* interpolate between \(s\sb{i}\) and \(s\sb{j}\) */
    j = (i+1) % (D+1);                 /* if \(i=D\), then \(j=0\); otherwise, \(j=i+1\) */

    si = tap2(D, w, q, i);             /* note, \(s\sb{i}(n) = x(n-i)\) */
    sj = tap2(D, w, q, j);             /* note, \(s\sb{j}(n) = x(n-j)\) */

    return si + (d - i) * (sj - si);
}

/* tapi.c - interpolated tap output of a delay line */


double tapi(int D, double *w, double *p, double d)                   /* usage: sd = tapi(D, w, p, d); */
{
       int i, j;
       double si, sj;

       i = (int) d;                       /* interpolate between \(s\sb{i}\) and \(s\sb{j}\) */
       j = (i+1) % (D+1);                 /* if \(i=D\), then \(j=0\); otherwise, \(j=i+1\) */

       si = tap(D, w, p, i);              /* note, \(s\sb{i}(n) = x(n-i)\) */
       sj = tap(D, w, p, j);              /* note, \(s\sb{j}(n) = x(n-j)\) */

       return si + (d - i) * (sj - si);
}



/* wrap.c - circular wrap of pointer p, relative to array w */

void wrap(int M, double *w, double **p)
{
    if (*p > w + M)  
        *p -= M + 1;          /* when \(*p=w+M+1\), it wraps around to \(*p=w\) */

    if (*p < w)  
        *p += M + 1;          /* when \(*p=w-1\), it wraps around to \(*p=w+M\) */
}


/* cdelay.c - circular buffer implementation of D-fold delay */

void cdelay(int D, double *w, double **p)
{
    (*p)--;                      /* decrement pointer and wrap modulo-\((D+1)\) */
    wrap(D, w, p);               /* when \(*p=w-1\), it wraps around to \(*p=w+D\) */
}

/* plain.c - plain reverberator with circular delay line */

double plain(int D, double *w, double **p, double a, double x) /* usage: y=plain(D,w,&p,a,x); */
{
    double y, sD;

    sD = tap(D, w, *p, D);                   /* \(D\)th tap delay output */
    y = x + a * sD;                          /* filter output */
    **p = y;                                 /* delay input */
    cdelay(D, w, p);                         /* update delay line */
    
    return y;
}

/* allpass.c - allpass reverberator with circular delay line */

double allpass(int D, double *w, double **p, double a, double x)                   /* usage: y=allpass(D,w,&p,a,x); */
{
    double y, s0, sD;

    sD = tap(D, w, *p, D);                   /* \(D\)th tap delay output */
    s0 = x + a * sD;
    y  = -a * s0 + sD;                       /* filter output */
    **p = s0;                                /* delay input */
    cdelay(D, w, p);                         /* update delay line */

    return y;
}



