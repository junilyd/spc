/**
 * Write .txt file from buffer.
 * @param buffer Pointer to input data.
 * @param name String containing path to output file.
 * @return void
 */
void smc_write_file_float(double buffer[], char *name, int len_buffer){
    FILE *OutFile;
    int n;

    OutFile=fopen(name, "w");
    if (OutFile) {
        for(n=0; n < len_buffer; n++)
        fprintf(OutFile, "%f\n", buffer[n]);
        fclose(OutFile);
    }
}

double * smc_sine(int fs, double fc, double dur, double A)
{
    int D, N;
    double *w, f1, q1;
    D = fs; N = fs*dur;
    double* y1 = malloc(sizeof(double)*N);
    w  = (double*)calloc(D,sizeof(double));

    q1 = 0; f1 = fc/D;
    int i;
    for (i = 0; i < D; i++) {
        w[(int)q1]= sine(D,i);
        gdelay2(D-1, 1.0, &q1);
    }
    int n;
    for (n=0; n < N; n++) {
        y1[n] = wavgeni(D, w, A, f1, &q1);
    }

    return y1;
}
