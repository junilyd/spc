#include <stdio.h>

void smc_write_file_float(double buffer[], char *name, int len_buffer) {
    FILE *OutFile;
    int n;

    OutFile=fopen(name, "w");
    if (OutFile) {
        for(n=0; n < len_buffer; n++)
        fprintf(OutFile, "%f\n", buffer[n]);
        fclose(OutFile);
    }
}