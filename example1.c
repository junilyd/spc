#include <stdio.h>
#include <stdlib.h>
#include "smc_library/smc_orfanidis.h"
#include "smc_library/smc.h"
#define output_file_x "data/convolution_input.txt" 
#define output_file_y "data/convolution_output.txt" 

int main() 
{
int n,L,K,M;
L = 200; //Length
K = 50;  //Samples
M = 14;
double x[L], y[L+M], h[M];
int i;
    for (i = 0; i < M; i++) {
        h[i] = 0.1;
    }
    for (n = 0; n<L; n++) {
        if (n%K < K/2) {
            x[n]=1;
        } else 
            x[n]=0;
    }
    // this is a function call from h-file
    conv(M, h, L, x, y);
    
    for (i = 0; i < M+L-1; i++) {
        printf("x=%lf , y=%lf, h=%lf \n",x[i],y[i],h[i]);
    }

    /* Function call from h-file -- File outputs*/ 
    int len; len = sizeof(x)/sizeof(x[0]); smc_write_file_float(x,output_file_x, len);
             len = sizeof(y)/sizeof(y[0]); smc_write_file_float(y,output_file_y, len);
 
    return 0;
}