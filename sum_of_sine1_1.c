#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smc_library/smc_orfanidis.h"
#include "smc_library/smc.h"
/* #define fs 44100 */
/* #define fc 110.0 */
/* #define dur 0.1 */
#define output_file_1 "data/sum_of_sine1_1.txt" 

int main()
{
    int fs, N; 
    double fc, dur, A;
    fs =44100;
    fc =10.0;
    dur = 1;
    A = 1;
    N = (int)fs*dur;
    double *y1, *y2, *y3, *y4, y[N];
    
    y1 = smc_sine(fs, fc , dur, A);
    y2 = smc_sine(fs, fc*2 , dur, A*0.5);
    y3 = smc_sine(fs, fc*3 , dur, A*0.35);
    y4 = smc_sine(fs, fc*4 , dur, A*0.1);
    
    int n;
    for (n = 0; n < N; n++) {
        y[n] = y1[n]+y2[n]+y3[n]+y4[n];
    }
    
    /* FILE OUTPUTS */
    smc_write_file_float(y,output_file_1, N);
return 0;
}
