function [H,w] = smc_peak(wc,dw,GBsquared)
    GB = (sqrt(GBsquared));

    beta = GB/sqrt(1-GB^2)*tan(dw/2);
    b = 1/(1+beta);
    w=linspace(0,2*pi,1000);

    num=[1-b 0 -(1-b)];
    den=[1 -2*b*cos(wc) (2*b-1)];

    H=freqz(num,den,w);
    
    figure(1); hold on;
        title(['\Delta\omega ',sprintf('= %1.2f',dw/pi),'\pi rad, ','\omega_c = ', sprintf('%1.2frad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude'); xlim([0 1]);
        plot(w/pi,((abs(H).^2)));%,w/pi,max(10*log10(abs(H.^2)))+10*log10(GB^2),'-r'); 


    figure(2); hold on;
        semilogx(w/pi,10*log10(((abs(H)).^2)),w/pi,10*log10(GB^2),'-r'); 
                title(['\Delta\omega ',sprintf('= %1.2f',dw/pi),'\pi rad, ','\omega_c = ', sprintf('%1.2f',wc/pi),'\pi rad, GB^2 = ',sprintf('%1.0f dB',10*log10(GB^2))] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude [dB]'); xlim([0 1]);
end
%clf;for i=1:4; [H,w]=smc_notch(pi/2,pi*0.1*i,0.5); hold on; end
