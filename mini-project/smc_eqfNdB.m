function [H,w] = smc_eq(fc,Q,G0,G,NdB)
    fs=44100;
    wc = fc*2*pi/fs;
    dw=fc*Q*2*pi/fs;
    G0=10^(G0/20);
    G=10^(G/20);


    GB = 10^(( 20*log10(G)*NdB )/20); 

    beta = sqrt((GB^2-G0^2)/(G^2-GB^2))*tan(dw/2);
    b = 1/(1+beta);

    w=linspace(0,2*pi,10000);
    f=w/2/pi*fs;
    num = [(G0+G*beta)*b (-2*G0*cos(wc))*b (G0-G*beta)*b];
    den = [1 (-2*cos(wc))*b (1-beta)*b];
    H=freqz(num,den,w);
    
    figure(4);
        semilogx(f,10*log10((abs(H).^2)),f,10*log10(GB^2),'-r'); hold on;
                % title(['\Deltaf ',sprintf('= %1.2f',df),'Hz, ','f_c = ', sprintf('%1.2f',fc),'Hz, NdB = ',sprintf('%1.4f',NdB)] ); xlabel('Frequency [Hz]'); ylabel('Magnitude [dB]'); xlim([20 20e3]);
                %% Q Demonstration
                % title(['\Deltaf= ',sprintf('= %1.2f',df),'Hz, ','Hz, NdB = ',sprintf('%1.4f',NdB)] ); xlabel('Frequency [Hz]'); ylabel('Magnitude [dB]'); xlim([20 20e3]);
                title(['Q=\omega_c/\Delta\omega= ',sprintf('%1.1f',Q),', NdB = ',sprintf('%1.4f',NdB)] ); xlabel('Frequency [Hz]'); ylabel('Magnitude [dB]'); xlim([20 20e3]);
                % LP HP filters
                % title(['\omega_c = 0rad and\omega_c=\pi rad,   NdB = ',sprintf('%1.4f',NdB)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude [dB]'); xlim([0 1]);
            % varying dw
                % title([sprintf('Notch Filter, GB^2 = %1.0fdB, ',10*log10(GB^2)),'\omega_c = ', sprintf('%1.2f rad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude [dB]'); xlim([0 1]); ylim([-70 0]);
            % varying GB
                % title(['Notch Filter \Delta\omega ',sprintf('= %1.2f',df),'\pi rad, ','\omega_c = ', sprintf('%1.2frad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude [dB]'); xlim([0.45 0.55]); ylim([-12 0]);

    set(gca,'ytick',[-600:6:600])
end

% close all;for i=1:9; [H,w]=smc_notch(pi/2,pi*i/10,0.5); hold on; end      % varying dw 
% close all;for i=1:9; [H,w]=smc_notch(pi/2,pi/10,0.1*i); hold on; end      % varying GB
% close all;for i=1:9; [H,w]=smc_notch(pi*i/10,pi/10,0.001); hold on; end   % varying wc
