function [H,w] = smc_eq(wc,dw,G0,G)
    
    GB = sqrt(G0*G);
    
    % GB = 10^(( 20*log10(G)*GB )/20); 
    fprintf('G = %1.4f, GB = %1.4f,      G = %1.4f, GB = %1.4f\n',G,GB,20*log10(G), 20*log10(GB));

    beta = sqrt((GB^2-G0^2)/(G^2-GB^2))*tan(dw/2);
    b = 1/(1+beta);

    w=linspace(0,2*pi,1000);
    num = [(G0+G*beta)*b (-2*G0*cos(wc))*b (G0-G*beta)*b];
    den = [1 (-2*cos(wc))*b (1-beta)*b];
    H=freqz(num,den,w);
    


    figure(1); hold on;
        semilogx(w/pi, abs(H.^2),w/pi,GB^2,'-r'); hold on;
                title(['\Delta\omega ',sprintf('= %1.2f',dw/pi),'\pi rad, ','\omega_c = ', sprintf('%1.2frad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude'); xlim([0 1]);
            % varying dw
                % title(sprintf('Notch Filter, GB^2 = %1.2e, w_c = %1.2f rad',GB^2,wc)); xlabel('Frequency [\pi rad]'); ylabel('Magnitude'); xlim([0 1]);% ylim([-4 0]);
            % varying GB
                % title(['Notch Filter \Delta\omega ',sprintf('= %1.2f',dw/pi),'\pi rad, ','\omega_c = ', sprintf('%1.2frad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude'); xlim([0 1]);

    figure(2); hold on;
        semilogx(w/pi,10*log10((abs(H).^2)),w/pi,10*log10(GB^2),'-r'); 
                title(['\Delta\omega ',sprintf('= %1.2f',dw/pi),'\pi rad, ','\omega_c = ', sprintf('%1.2f',wc/pi),'\pi rad, GB = \surd{G0*G}'] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude [dB]'); xlim([0 1]);
            % varying dw
                % title([sprintf('Notch Filter, GB^2 = %1.0fdB, ',10*log10(GB^2)),'\omega_c = ', sprintf('%1.2f rad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude [dB]'); xlim([0 1]); ylim([-70 0]);
            % varying GB
                % title(['Notch Filter \Delta\omega ',sprintf('= %1.2f',dw/pi),'\pi rad, ','\omega_c = ', sprintf('%1.2frad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude [dB]'); xlim([0.45 0.55]); ylim([-12 0]);

    set(gca,'ytick',[-600:6:600])
end

% close all;for i=1:9; [H,w]=smc_notch(pi/2,pi*i/10,0.5); hold on; end      % varying dw 
% close all;for i=1:9; [H,w]=smc_notch(pi/2,pi/10,0.1*i); hold on; end      % varying GB
% close all;for i=1:9; [H,w]=smc_notch(pi*i/10,pi/10,0.001); hold on; end   % varying wc
