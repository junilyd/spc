//
//  for_presentation.m
//  
//
//  Created by junilyd on 27/10/14.
//
//
// From skinSegmentation_CreateFiles.m
Classification errors are in RGB-space
qdc:      ldc:      nmsc:
0.0167    0.0684    0.1257
Classification errors are in HSV-space
qdc:      ldc:      nmsc:
0.0216    0.0499    0.0501
Classification errors are in HS-space
qdc:      ldc:      nmsc:
0.0300    0.0557    0.0549



// From skinSegmentationClassification.m
Classification errors are
nmsc: 0.0543

Confusion Table for NMSC

True     | Estimated Labels
Labels   | Non Sk Skin  | Totals
---------|--------------|-------
Non Skin |36201   2639  |38840
Skin     |   24  10148  |10172
---------|--------------|-------
Totals   |36225  12787  |49012


Classification errors are
ldc:  0.0551

Confusion Table for ldc

True     | Estimated Labels
Labels   | Non Sk Skin  | Totals
---------|--------------|-------
Non Skin |36162   2678  |38840
Skin     |   24  10148  |10172
---------|--------------|-------
Totals   |36186  12826  |49012



Classification errors are
qdc:
:    0.0147

Confusion Table for QDC

True     | Estimated Labels
Labels   | Non Sk Skin  | Totals
---------|--------------|-------
Non Skin |38143    697  |38840
Skin     |   24  10148  |10172
---------|--------------|-------
Totals   |38167  10845  |49012


Classification errors are
qdc with cost:
:    0.0145

Confusion Table for QDC

True     | Estimated Labels
Labels   | Non Sk Skin  | Totals
---------|--------------|-------
Non Skin |38152    688  |38840
Skin     |   24  10148  |10172
---------|--------------|-------
Totals   |38176  10836  |49012


%%  Read Image % I = imread('Ohmygod.jpg');% I = flipdim(I,1);
%% Take a picture
vid = videoinput('macvideo', 1);
vid.ReturnedColorSpace = 'rgb';
I = getsnapshot(vid);
subplot(121); imshow(I)
% imageinfo(I)
figure(4)
hsv = rgb2hsv(I);
H = hsv(:,:,1);
S = hsv(:,:,2);
V = hsv(:,:,3);
% First Removal
H(H>0.1 | H<0.02) = newVal;
S(S>0.8 | S<0.23)  = newVal;
hsv2 = cat(3, double(H), double(S), double(V));
RGB = hsv2rgb(hsv2);
subplot(122); imagesc(RGB)

Classification errors are
knnc
:    0.0029

Confusion Table for knnc with reduced data (z=12253 samples)

True     | Estimated Labels
Labels   | Non Sk Skin  | Totals
---------|--------------|-------
Non Skin | 1939      3  | 1942
Skin     |    4    505  |  509
---------|--------------|-------
Totals   | 1943    508  | 2451


