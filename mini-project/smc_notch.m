function [H,w] = smc_notch(wc,dw,GBsquared)
    GB = (sqrt(GBsquared));
    beta = sqrt(1-GB^2)/GB*tan(dw/2);
    b = 1/(1+beta);
    w=linspace(0,2*pi,1000); %z=exp(j*w);
    % Hnotch = b*(1-2*cos(wc)*z.^(-1)+z.^-2)/(1-2*b*cos(wc)*z.^(-1)+(2*b-1)*z.^(-2));
    num=[b -2*b*cos(wc) b];
    den=[1 -2*b*cos(wc) (2*b-1)];
    H=freqz(num,den,w);
    
    figure(1); hold on;
        semilogx(w/pi, abs(H.^2)); hold on;
                title(['\Delta\omega ',sprintf('= %1.2f',dw/pi),'\pi rad, ','\omega_c = ', sprintf('%1.2frad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude'); xlim([0 1]);
            % varying dw
                % title(sprintf('Notch Filter, GB^2 = %1.2e, w_c = %1.2f rad',GB^2,wc)); xlabel('Frequency [\pi rad]'); ylabel('Magnitude'); xlim([0 1]);% ylim([-4 0]);
            % varying GB
                % title(['Notch Filter \Delta\omega ',sprintf('= %1.2f',dw/pi),'\pi rad, ','\omega_c = ', sprintf('%1.2frad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude'); xlim([0 1]);

    figure(2); hold on;
        semilogx(w/pi,10*log10((abs(H).^2)),w/pi,10*log10(GB^2),'-r'); 
                title(['\Delta\omega ',sprintf('= %1.2f',dw/pi),'\pi rad, ','\omega_c = ', sprintf('%1.2f',wc/pi),'\pi rad, GB^2 = ',sprintf('%1.0f dB',10*log10(GB^2))] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude [dB]'); xlim([0 1]);
            % varying dw
                % title([sprintf('Notch Filter, GB^2 = %1.0fdB, ',10*log10(GB^2)),'\omega_c = ', sprintf('%1.2f rad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude [dB]'); xlim([0 1]); ylim([-70 0]);
            % varying GB
                % title(['Notch Filter \Delta\omega ',sprintf('= %1.2f',dw/pi),'\pi rad, ','\omega_c = ', sprintf('%1.2frad',wc)] ); xlabel('Frequency [\pi rad]'); ylabel('Magnitude [dB]'); xlim([0.45 0.55]); ylim([-12 0]);

    set(gca,'ytick',[-600:6:600])
end

% close all;for i=1:9; [H,w]=smc_notch(pi/2,pi*i/10,0.5); hold on; end      % varying dw 
% close all;for i=1:9; [H,w]=smc_notch(pi/2,pi/10,0.1*i); hold on; end      % varying GB
% close all;for i=1:9; [H,w]=smc_notch(pi*i/10,pi/10,0.001); hold on; end   % varying wc
