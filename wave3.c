/* Designed From exAMple on page 351 in Orfanidis */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smc_library/smc_orfanidis.h"
#include "smc_library/smc.h"
#define D 1000
#define N D*1
#define fc 0.02
#define AM 0.5*fc
#define A 1
#define FM 0.003
#define output_file_1 "data/wave3_f.txt" 
#define output_file_2 "data/wave3_y.txt" 

int main()
{
double *w, *wm, q, qm, y[N], f[N];
w  = (double*)calloc(D,sizeof(double));
wm = (double*)calloc(D,sizeof(double));

q = qm = 0;                                         // initialize offsets
int i;
for (i = 0; i < D; i++) {
    /* w[(int)q] = square(D/2,i); */
	/* wm[(int)qm] = trapez(D, D/4, 0, i); */
    w[(int)q] = sine(D,i);
	gdelay2(D-1, 1.0, &q);
   
    wm[(int)qm] = sine(D,i);
	gdelay2(D-1, 1.0, &qm);
    }

/* Generate Wave Signals */
int n;
for (n=0; n < N; n++) {
	f[n] = fc + wavgen(D, wm, AM, FM, &qm);
	y[n] = wavgen(D, w, A, f[n], &q);
    }

/* FILE OUTPUTS */
int len;
len = sizeof(f)/sizeof(f[0]); smc_write_file_float(f,output_file_1, len);
len = sizeof(y)/sizeof(y[0]); smc_write_file_float(y,output_file_2, len);

return 0;
}




