#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smc_library/smc_orfanidis.h"
#include "smc_library/smc.h"
#define output_file_1 "data/make_sine.txt" 

int main()
{
    int fs, N; 
    double fc, dur, A;
    fs =44100;
    fc =340.0;
    dur = 2;
    A = 0.3;
    N = (int)fs*dur;
    double *y;
    
    y = smc_sine(fs, fc , dur, A);
    
    /* FILE OUTPUTS */
    smc_write_file_float(y,output_file_1, N);
return 0;
}
