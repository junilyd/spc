all : main.c

example1 : example1.c  Makefile
	gcc -lm example1.c  -o executables/example1c
	./executables/example1c
	plots/plotfile data/convolution_input.txt data/convolution_output.txt

wave1 : wave1.c Makefile
	gcc -lm wave1.c -o executables/wave1
	./executables/wave1	
	plots/plotfile_wave1 data/wave1_F1.txt data/wave1_F2.txt data/wave1_F3.txt

wave2 : wave2.c Makefile
	gcc -lm wave2.c -o executables/wave2
	./executables/wave2	
	plots/plotfile_wave2 data/wave2_A.txt data/wave2_y.txt

wave3 : wave3.c Makefile
	gcc -lm wave3.c -o executables/wave3
	./executables/wave3	
	plots/plotfile_wave3 data/wave3_f.txt data/wave3_y.txt

make_sine : make_sine.c makefile
	gcc -lm make_sine.c -o executables/make_sine
	./executables/make_sine	
	plots/plotfile_make_sine data/make_sine.txt

sum_of_sine1_1 : sum_of_sine1_1.c makefile
	gcc -lm sum_of_sine1_1.c -o executables/sum_of_sine1_1
	./executables/sum_of_sine1_1	
	plots/plotfile_sum_of_sine1_1 data/sum_of_sine1_1.txt

# REVERB

reverb1 : reverb1.c makefile
	gcc -lm reverb1.c -o executables/reverb1
	./executables/reverb1	
	plots/plotfile_reverb1 data/input.txt data/plain.txt data/allpass.txt
