
/* TODO: define 
 *       w and p
 *       D
 *       a
 *       */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smc_library/smc_orfanidis.h"
#include "smc_library/smc.h"
#define output_file_1 "data/input.txt"
#define output_file_2 "data/plain.txt"
#define output_file_3 "data/allpass.txt"

/* TODO: Check that the 2 reverbs actually works 
 *       understand how to make D1 and D2
 *       understand also w1 w2
 *       understand p1 p2
 *       */
int main() 
{

    int fs, D, N, Nrev;
    /* int D1, D2; D1 = (int)fs*0.1; D2 = (int)fs*0.5; // Test case */
    double fc, dur, A;
    fs =44100; fc =10.0; dur = 2; A = 0.3; D = fs; N = (int)fs*dur; Nrev = N*3;
    double *x, y1[N], y2[N], a;
    a = 0.3;

    double *w1, *w2, *p1, *p2;
    w1 = (double *) calloc(D+1, sizeof(double));
    w2 = (double *) calloc(D+1, sizeof(double));
    p1 = w1; p2 = w2;
    
    x = smc_sine(fs, fc , dur, A);
   
    int n;
    for (n = 0; n < N; n++) {
        y1[n] = plain(D, w1, &p1, a, x[n]);
        y2[n] = allpass(D, w2, &p2, a, x[n]);
    }
    
    /* FILE OUTPUT */
    int len;
    len = sizeof(x)/sizeof(x[0]); smc_write_file_float(y1,output_file_1, N);
    len = sizeof(y1)/sizeof(y1[0]); smc_write_file_float(y1,output_file_2, N);
    len = sizeof(y2)/sizeof(y2[0]); smc_write_file_float(y1,output_file_3, N);

    return 0;
}
