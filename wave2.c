/* Designed From example on page 351 in Orfanidis */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smc_library/smc_orfanidis.h"
#include "smc_library/smc.h"
#define D 1000
#define N D*1
#define AM 0.2
#define output_file_y1 "data/wave2_A.txt" 
#define output_file_y2 "data/wave2_y.txt" 

int main()
{
double *w, *wenv, q, qenv, m, Aenv, f, fenv, A[N],y[N];
w    = (double*)calloc(D,sizeof(double));
wenv = (double*)calloc(D,sizeof(double));

q = qenv = 0;                                         // initialize offsets
int i;
for (i = 0; i < D; i++) {
    w[(int)q] = sine(D,i);
    wenv[(int)q] = 1-AM+AM*sine(D,i);
    /* wenv[(int)q] = 1-AM+AM*square(D/2,i); */
	/* wenv[(int)qenv] = trapez(D, D/4, 0, i); */
	gdelay2(D-1, 1.0, &q);
	gdelay2(D-1, 1.0, &qenv);
    }

fenv = 1.0/N; Aenv = 1; f = 0.02;
/* Generate Wave Signals */
int n;
for (n=0; n < N; n++) {
	A[n] = wavgen(D, wenv, Aenv, fenv, &qenv);
	y[n] = wavgen(D, w,A[n], f, &q);
    }

/* FILE OUTPUTS */
int len;
len = sizeof(A)/sizeof(A[0]); smc_write_file_float(A,output_file_y1, len);
len = sizeof(y)/sizeof(y[0]); smc_write_file_float(y,output_file_y2, len);

return 0;
}




