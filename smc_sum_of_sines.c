#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "smc_library/smc_orfanidis.h"
#include "smc_library/smc.h"
/* #define fs 44100 */
/* #define fc 110.0 */
/* #define dur 0.1 */
#define output_file_1 "data/sum_of_sine1_1.txt" 
// TODO: Needs to have phaseshift and functionality of adding sines
void smc_sum_of_sines(int fs, double fc, double dur, int A)
{
int D, N;
double *w, f1, q1;
D = fs; 
N = fs*dur; A = 1;
double y1[N];
w = (double*)calloc(D,sizeof(double));

q1 = 0; f1 = fc/D;
for (int i = 0; i < D; i++) {
	w[(int)q1]	= sine(D,i);
	gdelay2(D-1, 1.0, &q1);
}
for (int n=0; n < N; n++) {
	y1[n] = wavgeni(D, w, A, f1, &q1);
}
	

/* FILE OUTPUTS */
int len;
len = sizeof(y1)/sizeof(y1[0]); smc_write_file_float(y1,output_file_1, len);

/* return 0; */
}
